package com.example.currencies.main.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.currencies.data.model.entity.Currency

class CurrencyDiffCallback : DiffUtil.ItemCallback<Currency>() {

    companion object {
        const val VALUE_CHG = "VALUE_CHG"
    }

    override fun areItemsTheSame(
        oldItem: Currency,
        newItem: Currency
    ): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(
        oldItem: Currency,
        newItem: Currency
    ): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: Currency, newItem: Currency): Any? {
        val payloadSet = mutableSetOf<String>()

        if (oldItem.rate != newItem.rate)
            payloadSet.add(VALUE_CHG)

        return payloadSet
    }
}