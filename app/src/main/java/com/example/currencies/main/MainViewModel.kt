package com.example.currencies.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.currencies.data.model.entity.Currency
import com.example.currencies.data.repository.currency.CurrencyRepository
import kotlinx.coroutines.launch


class MainViewModel(
    private val currencyRepository: CurrencyRepository
) : ViewModel() {

    private val _baseValue: MutableLiveData<Float> = MutableLiveData()
    private val _baseCurrency: MutableLiveData<String> = MutableLiveData()

    val networkErrors = currencyRepository.networkErrors

    init {
        _baseCurrency.value = "EUR"
        _baseValue.value = 100f
    }

    fun fetchCurrency() {
        viewModelScope.launch {
            currencyRepository.fetchCurrency(_baseCurrency.value!!, _baseValue.value!!)
        }
    }

    fun getCurrencyList(): LiveData<List<Currency>?> {
        return currencyRepository.getCurrencyList(_baseCurrency.value!!)
    }

    fun setBaseCurrency(base: String, newBaseValue: Float) {
        _baseValue.value = newBaseValue
        _baseCurrency.value = base
        fetchCurrency()
    }

    fun setNewBaseValue(value: Float) {
        _baseValue.value = value
    }
}