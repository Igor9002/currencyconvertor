package com.example.currencies.main

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.example.currencies.data.repository.currency.CurrencyRepository
import org.koin.dsl.module

val mainModule = module {
    factory { (fragmentActivity: FragmentActivity) ->
        provideViewModel(fragmentActivity, get())
    }
}

private fun provideViewModel(
    fragmentActivity: FragmentActivity,
    currencyRepository: CurrencyRepository
): MainViewModel {
    return ViewModelProvider(
        fragmentActivity,
        MainViewModelFactory(currencyRepository)
    ).get(MainViewModel::class.java)
}