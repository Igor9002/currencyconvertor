package com.example.currencies.main.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import com.example.currencies.R
import com.example.currencies.binding.DataBindingAdapter
import com.example.currencies.data.model.entity.Currency
import com.example.currencies.databinding.CurrencyItemBinding


class CurrencyAdapter(
    private val dataBindingComponent: DataBindingComponent,
    private val onCurrencyEdited: ((Float) -> Unit)?,
    private val onBaseChanged: (Currency) -> Unit
) :
    DataBindingAdapter<Currency, CurrencyItemBinding>(CurrencyDiffCallback()) {

    private val valueWatcher: TextWatcher

    init {
        this.valueWatcher = object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(newValue: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(newValue: Editable?) {
                val value = try {
                    newValue.toString().trim().toFloat()
                } catch (e: Exception) {
                    0F
                }
                currentList[0].value = value
                onCurrencyEdited?.invoke(value)
            }
        }
    }

    override fun createBinding(parent: ViewGroup): CurrencyItemBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.currency_item,
            parent,
            false,
            dataBindingComponent
        )
    }

    override fun bind(binding: CurrencyItemBinding, item: Currency, position: Int) {
        binding.item = item
        binding.currencyAmount.removeTextChangedListener(valueWatcher)
        binding.currencyAmount.isEnabled = position == 0
        if (position == 0) {
            binding.currencyAmount.addTextChangedListener(valueWatcher)
        } else {
            binding.root.setOnClickListener {
                binding.item?.let {
                    onBaseChanged.invoke(it)
                }
            }
        }
    }
}