package com.example.currencies.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.currencies.data.repository.currency.CurrencyRepository

class MainViewModelFactory(
    private val currencyRepository: CurrencyRepository
) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == MainViewModel::class.java) { "Unknown ViewModel class" }
        return MainViewModel(currencyRepository) as T
    }
}