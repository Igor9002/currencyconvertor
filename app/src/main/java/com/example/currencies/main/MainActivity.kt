package com.example.currencies.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingComponent
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.currencies.R
import com.example.currencies.binding.ActivityDataBindingComponent
import com.example.currencies.data.model.entity.Currency
import com.example.currencies.extentions.showToast
import com.example.currencies.main.adapter.CurrencyAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class    MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by inject { parametersOf(this) }
    private var dataBindingComponent: DataBindingComponent = ActivityDataBindingComponent()
    private lateinit var adapter: CurrencyAdapter
    private var isScreenActive = false

    companion object {
        private const val SECOND_IN_MILLIS: Long = 1000
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()

        viewModel.getCurrencyList().observe(this, Observer { currency ->
            adapter.submitList(currency)
        })

        viewModel.networkErrors.observe(this, Observer {
            showToast(it)
        })
    }

    override fun onResume() {
        super.onResume()
        isScreenActive = true
        lifecycleScope.launch {
            refreshRates()
        }
    }

    private fun setupRecyclerView() {
        adapter = CurrencyAdapter(dataBindingComponent, {
            viewModel.setNewBaseValue(it)
            updateBaseValue(it)
        }, {
            setNewBase(it)
        })
        ratesRecyclerView.adapter = adapter
    }

    private fun updateBaseValue(value: Float) {
        val newCurrencyRates: MutableList<Currency> = mutableListOf()
        adapter.currentList.forEach {
            newCurrencyRates.add(Currency(it.name, it.rate, it.rate * value))
        }
        adapter.submitList(newCurrencyRates)
    }

    private fun setNewBase(currency: Currency) {
        viewModel.setBaseCurrency(currency.name, currency.value)
        ratesRecyclerView.smoothScrollToPosition(0)
    }

    private suspend fun refreshRates() = withContext(Dispatchers.Main) {
        while (isScreenActive) {
            delay(SECOND_IN_MILLIS)
            viewModel.fetchCurrency()
        }
    }

    override fun onStop() {
        super.onStop()
        isScreenActive = false
    }
}