package com.example.currencies.extentions

import android.content.Context
import android.widget.Toast

/**
 * Extension function for showing a toast message for a short period [Toast.LENGTH_SHORT]
 * @param message The message that should appear on the screen
 */
fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}