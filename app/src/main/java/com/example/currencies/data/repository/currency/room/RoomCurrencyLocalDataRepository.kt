package com.example.currencies.data.repository.currency.room

import androidx.lifecycle.LiveData
import com.example.currencies.data.model.entity.Currency
import com.example.currencies.data.model.repository.CurrencyLocalDataRepository
import com.example.currencies.data.repository.currency.room.dao.CurrencyDao

class RoomCurrencyLocalDataRepository(private val currencyDao: CurrencyDao) :
    CurrencyLocalDataRepository {

    override fun saveCurrencyList(currencyList: List<Currency>?) {
        return currencyDao.insertCurrencyList(currencyList)
    }

    override fun getCurrencyList(base: String): LiveData<List<Currency>?> {
        return currencyDao.getCurrencyList()
    }
}