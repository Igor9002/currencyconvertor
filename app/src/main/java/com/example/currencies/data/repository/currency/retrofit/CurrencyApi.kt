package com.example.currencies.data.repository.currency.retrofit

import com.example.currencies.data.model.entity.LatestRates
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("/latest")
    suspend fun getCurrency(@Query("base") baseCurrency: String): LatestRates
}