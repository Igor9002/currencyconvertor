package com.example.currencies.data.repository.currency

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.currencies.data.datasource.NetworkDataSource
import com.example.currencies.data.model.entity.Currency
import com.example.currencies.data.model.entity.LatestRates
import com.example.currencies.data.model.repository.CurrencyLocalDataRepository
import com.example.currencies.data.repository.currency.retrofit.CurrencyApi
import com.qubiz.qbuzz.data.coroutines.CoroutinesDispatcherProvider
import kotlinx.coroutines.withContext

class CurrencyRepository(
    private val currencyLocalDataRepository: CurrencyLocalDataRepository,
    private val currencyApi: CurrencyApi,
    private val networkDataSource: NetworkDataSource,
    private val dispatcherProvider: CoroutinesDispatcherProvider
) {

    private val _networkErrors = MutableLiveData<String>()
    private val rates: MutableList<Currency> = mutableListOf()
    val networkErrors: LiveData<String>
        get() = _networkErrors

    suspend fun fetchCurrency(baseCurrency: String, baseValue: Float) {
        withContext(dispatcherProvider.io) {
            networkDataSource.getResult(
                {
                    rates.clear()
                    currencyApi.getCurrency(baseCurrency)
                },
                { latestRates ->
                    saveCurrencyList(latestRates, baseValue)
                },
                { error ->
                    _networkErrors.postValue(error)
                })
        }
    }

    fun getCurrencyList(base: String): LiveData<List<Currency>?> {
        return currencyLocalDataRepository.getCurrencyList(base)
    }

    private fun saveCurrencyList(latestRates: LatestRates, baseValue: Float) {
        rates.add(Currency(latestRates.base!!, 1.0f, baseValue))
        latestRates.rates?.forEach { (name, rate) ->
            rates.add(Currency(name, rate, rate * baseValue))
        }
        currencyLocalDataRepository.saveCurrencyList(rates)
    }
}