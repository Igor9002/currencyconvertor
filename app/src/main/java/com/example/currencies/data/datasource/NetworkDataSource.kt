package com.example.currencies.data.datasource

import com.example.currencies.data.model.entity.LatestRates
import retrofit2.HttpException

class NetworkDataSource {
    suspend fun getResult(
        call: suspend () -> LatestRates,
        onSuccess: (currency: LatestRates) -> Unit,
        onError: (error: String) -> Unit
    ) {
        return try {
            onSuccess(call())
        } catch (e: HttpException) {
            onError("Error code: ${e.code()}, Error message: ${e.message()}")
        } catch (e: Exception) {
            onError("Error message: ${e.message}")
        }
    }
}