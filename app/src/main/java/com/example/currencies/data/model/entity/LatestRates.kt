package com.example.currencies.data.model.entity

class LatestRates {
    val base: String? = null
    val date: String? = null
    val rates: Map<String, Float>? = null
}