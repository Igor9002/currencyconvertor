package com.example.currencies.data.model.repository

import androidx.lifecycle.LiveData
import com.example.currencies.data.model.entity.Currency

interface CurrencyLocalDataRepository {

    fun saveCurrencyList(currencyList: List<Currency>?)

    fun getCurrencyList(base: String): LiveData<List<Currency>?>

}