package com.example.currencies.data.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency")
data class Currency(
    @PrimaryKey
    val name: String,
    val rate: Float,
    var value: Float
)