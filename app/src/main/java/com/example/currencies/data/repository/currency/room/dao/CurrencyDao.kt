package com.example.currencies.data.repository.currency.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.currencies.data.model.entity.Currency

@Dao
interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrencyList(currencyList: List<Currency>?)

    @Query("SELECT * FROM currency")
    fun getCurrencyList(): LiveData<List<Currency>?>
}