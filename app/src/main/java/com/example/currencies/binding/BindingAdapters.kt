package com.example.currencies.binding

import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import com.example.currencies.R
import java.util.*

@BindingAdapter("currencyAmount")
fun bindCurrencyAmount(editText: EditText, amount: Float) {
    val amountValue = "%.2f".format(amount)
    editText.setText(amountValue)
}

@BindingAdapter("currencyFlag")
fun getCurrencyFlag(imageView: ImageView, currencyName: String) {
    val resources = imageView.context.resources
    val packageName = imageView.context.packageName
    val drawableName = currencyName.substring(0, 2).toLowerCase(Locale.getDefault())

    var currencyFlag = try {
        resources.getIdentifier(drawableName, "drawable", packageName)
    } catch (e: Exception) {
        0
    }

    if (currencyFlag == 0 || currencyName == "EUR")
        currencyFlag = R.drawable.eu
    val flagDrawable = ResourcesCompat.getDrawable(imageView.resources, currencyFlag, null)
    imageView.setImageDrawable(flagDrawable)
}

@BindingAdapter("currencyDescription")
fun getCurrencyDesc(textView: TextView, currencyName: String) {
    val resources = textView.context.resources
    val packageName = textView.context.packageName

    val currencyDescName = try {
        resources.getString(resources.getIdentifier(currencyName, "string", packageName))
    } catch (e: Exception) {
        "N/A"
    }
    textView.text = currencyDescName
}

