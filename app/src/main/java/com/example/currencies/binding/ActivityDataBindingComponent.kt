package com.example.currencies.binding

import androidx.databinding.DataBindingComponent

class ActivityDataBindingComponent : DataBindingComponent {
    private val adapter = ActivityBindingAdapter()

    fun getActivityBindingAdapters() = adapter
}