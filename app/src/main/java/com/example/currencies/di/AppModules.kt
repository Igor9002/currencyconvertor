package com.example.currencies.di

import com.example.currencies.data.datasource.NetworkDataSource
import com.example.currencies.data.model.repository.CurrencyLocalDataRepository
import com.example.currencies.data.repository.currency.CurrencyRepository
import com.example.currencies.data.repository.currency.retrofit.CurrencyApi
import com.example.currencies.data.repository.currency.room.RoomCurrencyLocalDataRepository
import com.example.currencies.data.repository.currency.room.dao.CurrencyDao
import com.example.currencies.main.mainModule
import com.qubiz.qbuzz.data.coroutines.CoroutinesDispatcherProvider
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit

private val dataModules = module {
    single { provideCurrencyApi(get()) }
    single { provideNetworkDataSource() }
    single { provideCurrencyLocalDataRepository(get()) }
    single { provideCurrencyRepository(get(), get(), get(), get()) }

}

private val baseModules: List<Module> =
    listOf(coroutinesModule, roomModule, retrofitNetworkModule)


val appModules: List<Module> = listOf(dataModules) + baseModules + mainModule

private fun provideCurrencyApi(retrofitInstance: Retrofit): CurrencyApi {
    return retrofitInstance.create(CurrencyApi::class.java)
}

private fun provideNetworkDataSource(): NetworkDataSource {
    return NetworkDataSource()
}

private fun provideCurrencyLocalDataRepository(currencyDao: CurrencyDao): CurrencyLocalDataRepository {
    return RoomCurrencyLocalDataRepository(currencyDao)
}

private fun provideCurrencyRepository(
    currencyLocalDataRepository: CurrencyLocalDataRepository,
    currencyApi: CurrencyApi,
    networkDataSource: NetworkDataSource,
    dispatcherProvider: CoroutinesDispatcherProvider
): CurrencyRepository {
    return CurrencyRepository(
        currencyLocalDataRepository,
        currencyApi,
        networkDataSource,
        dispatcherProvider
    )
}