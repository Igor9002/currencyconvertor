package com.example.currencies.di

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.currencies.data.model.entity.Currency
import com.example.currencies.data.repository.currency.room.dao.CurrencyDao
import org.koin.dsl.module

val roomModule = module {
    single { provideDatabaseInstance(get()) }
    single { provideCurrencyDao(get()) }
}

private object DatabaseProperties {
    const val DATABASE_VERSION = 1
}

private fun provideDatabaseInstance(applicationContext: Context): AppDatabase {
    return AppDatabase.getAppDataBase(applicationContext)
}

private fun provideCurrencyDao(appDatabase: AppDatabase): CurrencyDao {
    return appDatabase.currencyDao()
}

@Database(
    entities = [Currency::class],
    version = DatabaseProperties.DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun currencyDao(): CurrencyDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room
                        .databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java,
                            "currencyDB"
                        )
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE!!
        }
    }
}