package com.example.currencies.di

import com.qubiz.qbuzz.data.coroutines.CoroutinesDispatcherProvider
import org.koin.dsl.module

val coroutinesModule = module {
    single { CoroutinesDispatcherProvider() }
}